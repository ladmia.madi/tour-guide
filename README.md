# Tour-Guide

Projet 8 Openclassrooms Améliorez votre application avec des systèmes distribués

# Architecture

![Architecture](https://gitlab.com/ladmia.madi/tour-guide/-/blob/main/architecture-tourGuide.png)

# Documentation

![Documentation](https://gitlab.com/ladmia.madi/tour-guide/-/blob/main/Madi_Ladmia_2_documentation_042022.pdf)
