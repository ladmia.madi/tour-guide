package tourGuide.service;

import com.google.common.util.concurrent.RateLimiter;
import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.springframework.stereotype.Service;
import tourGuide.user.User;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.*;

@Service
public class GpsUtilService {
    private final GpsUtil gpsUtil;
    private ExecutorService service = Executors.newFixedThreadPool(100000);

    public GpsUtilService(GpsUtil gpsUtil) {
        this.gpsUtil = gpsUtil;
    }


    public VisitedLocation getUserLocation(UUID userId) {
        this.sleep();
        double longitude = ThreadLocalRandom.current().nextDouble(-180.0D, 180.0D);
        double latitude = ThreadLocalRandom.current().nextDouble(-85.05112878D, 85.05112878D);
        return new VisitedLocation(userId, new Location(latitude, longitude), new Date());
    }

    private void sleep() {
        int random = ThreadLocalRandom.current().nextInt(30, 100);

        try {
            TimeUnit.MILLISECONDS.sleep((long)random);
        } catch (InterruptedException var3) {
        }

    }

    public void addUserLocation(User user, RewardsService rewardsService) {
            CompletableFuture.supplyAsync(() -> getUserLocation(user.getUserId()), service)
                        .thenAccept(user::addToVisitedLocations)
                    .thenRun(() -> rewardsService.calculateRewards(user));
    }

    public List<Attraction> getAttractions() {
        return gpsUtil.getAttractions();
    }
}
