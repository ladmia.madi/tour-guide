package tourGuide.service;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import rewardCentral.RewardCentral;
import tourGuide.user.User;
import tourGuide.user.UserReward;

@Service
public class RewardsService {
    private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;

	private int proximityBuffer = Integer.MAX_VALUE;
	private final RewardCentral rewardsCentral;
	private final ExecutorService service = Executors.newCachedThreadPool();
	private final GpsUtilService gpsUtilService;

	
	public RewardsService(RewardCentral rewardCentral, GpsUtilService gpsUtilService) {
		this.rewardsCentral = rewardCentral;
		this.gpsUtilService = gpsUtilService;
	}
	
	public void setProximityBuffer(int proximityBuffer) {
		this.proximityBuffer = proximityBuffer;
	}

	public boolean isWithinAttractionProximity(Attraction attraction, Location location) {
		int attractionProximityRange = 200;
		return (getDistance(attraction, location) <= attractionProximityRange);
	}

	public void calculateRewards(User user) {
		List<Attraction> attractions = gpsUtilService.getAttractions();
		List<VisitedLocation> visitedLocationList = user.getVisitedLocations();

		visitedLocationList.stream().parallel()
				.forEach(visitedLocation -> attractions.stream().filter(
						attraction -> user.getUserRewards().stream().noneMatch(r -> r.attraction.attractionName.equals(attraction.attractionName))
				).parallel().forEach(
						attraction -> calculateDistanceReward(user, visitedLocation, attraction)
				));

	}

	public void calculateDistanceReward(User user, VisitedLocation visitedLocation, Attraction attraction) {
		double distance = getDistance(attraction, visitedLocation.location);
		if(distance <= proximityBuffer) {
			UserReward userReward = new UserReward(visitedLocation, attraction, (int) distance);
			addRewardPointsToUser(userReward, attraction, user);
		}
	}

	private void addRewardPointsToUser(UserReward userReward, Attraction attraction, User user) {
		CompletableFuture.supplyAsync(() -> rewardsCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId())
				, service)
				.thenAccept(points -> {
					userReward.setRewardPoints(points);
					user.addUserReward(userReward);
				});
	}

	public int getAttractionRewardPoint(Attraction attraction, User user) {
		return rewardsCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId());
	}

	public List<Attraction> getSortedAttractions (List<Attraction> attractions, VisitedLocation visitedLocation) {
		attractions.sort((o1, o2) -> (int) (getDistance(o1, visitedLocation.location) - getDistance(o2, visitedLocation.location)));

		return attractions.stream().limit(5).collect(Collectors.toList());
	}
	
	public double getDistance(Location loc1, Location loc2) {
        double lat1 = Math.toRadians(loc1.latitude);
        double lon1 = Math.toRadians(loc1.longitude);
        double lat2 = Math.toRadians(loc2.latitude);
        double lon2 = Math.toRadians(loc2.longitude);
        double angle = Math.acos(Math.sin(lat1) * Math.sin(lat2)
                               + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

        double nauticalMiles = 60 * Math.toDegrees(angle);
		return STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
	}
}
